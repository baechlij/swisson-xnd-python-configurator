#!/usr/bin/env python3

__author__ = "Jonas Baechli"
__version__ = "1.0.1"

import requests
import argparse

TIMEOUT = 1.0

PROTOCOL_ARTNET = 1
PROTOCOL_SACN = 2
DIRECTION_IN = 0
DIRECTION_OUT = 1
OUTPUT_MODE_MAX = 0
OUTPUT_MODE_RELAXED = 1
MERGE_MODE_HTP = 0
MERGE_MODE_LTP = 1
MERGE_MODE_OFF = 2
FAILURE_BEHAVIOUR_OFF = 0
FAILURE_BEHAVIOUR_HOLD = 1
		
def getPortModelUrl(ipAddr, port):
	return "http://" + str(ipAddr) + "/model/port" + str(port)
	
def getXndTypeForIp(ipAddr):
	try:
		url = "http://" + str(ipAddr) + "/model/device"
		r = requests.get(url, timeout = TIMEOUT, verify=False)
		deviceModel = r.json()
		return deviceModel["modelName"]
	except Exception as e:
		print("Request Failed: " + str(e))

def getNumberOfPortForXndType(xndType):
	if xndType == "XND-4":
		return 4
	elif xndType == "XND-8":
		return 8
	else:
		raise Exception("XND Type " + str(xndType) + " not supported")
		
def printPort(ipAddr, port):
	try:
		r = requests.get(getPortModelUrl(ipAddr, port), timeout = TIMEOUT, verify=False)
		portResponse = r.json()
		isEnabled = portResponse["isEnabled"]
		direction = "In" if portResponse["direction"]==DIRECTION_IN else "Out"
		mode = "Off" if not isEnabled else direction
		protocol = "Art-Net" if portResponse["protocol"]==PROTOCOL_ARTNET else "sACN"
		universe = portResponse["universe"]
		outputMode = "Max" if portResponse["outputMode"]==OUTPUT_MODE_MAX else "Relaxed"
		mergeMode = "Off" if portResponse["mergeMode"]==MERGE_MODE_OFF else "LTP" if portResponse["mergeMode"]==MERGE_MODE_LTP else "HTP" 
		failureBehaviour = "Off" if portResponse["failureBehaviour"]==FAILURE_BEHAVIOUR_OFF else "Hold"
		artNetRdm = "Enabled" if portResponse["artNetRdm"] else "Disabled"
		autoUniLabel = "Enabled" if portResponse["autoUniLabel"] else "Disabled"
		print("{} => Port {}:\n\tMode: {}\n\tProtocol: {}\n\tUniverse: {}\n\tOutput Mode: {}\n\tMerge Mode: {}\n\tFailure Behaviour: {}\n\tArt-Net RDM: {}\n\tAuto Universe Label: {}".format(ipAddr, port, mode, protocol, universe, outputMode, mergeMode, failureBehaviour, artNetRdm, autoUniLabel))
	except Exception as e:
		print("Request Failed: " + str(e))
		
def setPortUniverse(ipAddr, port, universe, verbose=True):
	try:
		r = requests.post(getPortModelUrl(ipAddr, port), json={"universe" : universe}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the universe for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
		elif verbose:
			print("{} => Port {}: Universe: {}".format(ipAddr, port, universe))
	except Exception as e:
		print("Request Failed: " + str(e))
		
def setPortProtocol(ipAddr, port, protocol, verbose=True):
	try:
		protocolUint = 0
		if protocol == "Art-Net":
			protocolUint = PROTOCOL_ARTNET
		elif protocol == "sACN":
			protocolUint = PROTOCOL_SACN
		else:
			raise Exception("Protocol " + str(protocol) + " is not supported")
		r = requests.post(getPortModelUrl(ipAddr, port), json={"protocol" : protocolUint}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the protocol for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
		elif verbose:
			print("{} => Port {}: Protocol: {}".format(ipAddr, port, protocol))
	except Exception as e:
		print("Request Failed: " + str(e))
	
def setPortMode(ipAddr, port, mode, verbose=True):
	def setPortEnabled(ipAddr, port, isEnabled):
		r = requests.post(getPortModelUrl(ipAddr, port), json={"isEnabled" : isEnabled}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting mode for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
	
	def setPortDirection(ipAddr, port, direction):
		r = requests.post(getPortModelUrl(ipAddr, port), json={"direction" : direction}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the direction for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
			
	try:
		if mode == "In":
			setPortEnabled(ipAddr, port, True)
			setPortDirection(ipAddr, port, DIRECTION_IN)
		elif mode == "Out":
			setPortEnabled(ipAddr, port, True)
			setPortDirection(ipAddr, port, DIRECTION_OUT)
		elif mode == "Off":
			setPortEnabled(ipAddr, port, False)
		else:
			raise Exception("Mode " + str(mode) + " is not supported")
		
		if verbose:
			print("{} => Port {}: Mode: {}".format(ipAddr, port, mode))
	except Exception as e:
		print("Request Failed: " + str(e))
		
def setPortArtNetRdmEnabled(ipAddr, port, isEnabled, verbose=True):
	try:
		r = requests.post(getPortModelUrl(ipAddr, port), json={"artNetRdm" : isEnabled}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the Art-Net RDM for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
		elif verbose:
			print("{} => Port {}: ArtNetRdm: {}".format(ipAddr, port, isEnabled))
	except Exception as e:
		print("Request Failed: " + str(e))
		
def setPortOutputMode(ipAddr, port, outputMode, verbose=True):
	try:
		outputModeUint = 0
		if outputMode == "Max":
			outputModeUint = OUTPUT_MODE_MAX
		elif outputMode == "Relaxed":
			outputModeUint = OUTPUT_MODE_RELAXED
		else:
			raise Exception("Output Mode " + str(outputMode) + " is not supported")
		r = requests.post(getPortModelUrl(ipAddr, port), json={"outputMode" : outputModeUint}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the Output Mode for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
		elif verbose:
			print("{} => Port {}: Output Mode: {}".format(ipAddr, port, outputMode))
	except Exception as e:
		print("Request Failed: " + str(e))

def setPortMergeMode(ipAddr, port, mergeMode, verbose=True):
	try:
		mergeModeUint = 0
		if mergeMode == "HTP":
			mergeModeUint = MERGE_MODE_HTP
		elif mergeMode == "LTP":
			mergeModeUint = MERGE_MODE_LTP
		elif mergeMode == "Off":
			mergeModeUint = MERGE_MODE_OFF
		else:
			raise Exception("Merge Mode " + str(mergeMode) + " is not supported")
		r = requests.post(getPortModelUrl(ipAddr, port), json={"mergeMode" : mergeModeUint}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the Merge Mode for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
		elif verbose:
			print("{} => Port {}: Merge Mode: {}".format(ipAddr, port, mergeMode))
	except Exception as e:
		print("Request Failed: " + str(e))
		
def setPortFailureBehaviour(ipAddr, port, failureBehaviour, verbose=True):
	try:
		failureBehaviourUint = 0
		if failureBehaviour == "Hold":
			failureBehaviourUint = FAILURE_BEHAVIOUR_HOLD
		elif failureBehaviour == "Off":
			failureBehaviourUint = FAILURE_BEHAVIOUR_OFF
		else:
			raise Exception("Failure Behaviour " + str(failureBehaviour) + " is not supported")
		r = requests.post(getPortModelUrl(ipAddr, port), json={"failureBehaviour" : failureBehaviourUint}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the Failure Behaviour for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
		elif verbose:
			print("{} => Port {}: Failure Behaviour: {}".format(ipAddr, port, failureBehaviour))
	except Exception as e:
		print("Request Failed: " + str(e))
		
def setPortAutoUniLabel(ipAddr, port, isEnabled, verbose=True):
	try:
		r = requests.post(getPortModelUrl(ipAddr, port), json={"autoUniLabel" : isEnabled}, timeout = TIMEOUT)
		if not (r.status_code == 200 and r.json()["status"]=="OK"):
			print("Error when setting the Auto Universe Label for {} for port {}: {}".format(ipAddr, port, r.json()["message"]))
		elif verbose:
			print("{} => Port {}: Auto Universe Label: {}".format(ipAddr, port, isEnabled))
	except Exception as e:
		print("Request Failed: " + str(e))
		
def checkAllIpsAreXnds(ipAddrs):
	for ipAddr in ipAddrs:
		if None == getXndTypeForIp(ipAddr):
			print()
			print("ERROR: {} is either offline or not a XND device.".format(ipAddr))
			print("Exit")
			print()
			exit(-1)

parser = argparse.ArgumentParser(description='Remote configuration utility for Swisson XND-4 and XND-8.')
parser.add_argument("--print", action="store_true", help="If given, the current configuration of the listed nodes is printed and all other flags are ignored.")
parser.add_argument("ipAddrs", metavar="IP", nargs="+", help="At least one IP address in the order in which they are to be configured/printed.")
parser.add_argument("-u", "--universe", type=int, help="The first universe.")
parser.add_argument("--noAutoIncrement", action="store_true", help="If set, all ports are configured identical, otherwise universe is incremented by 1 for each port.")
parser.add_argument("--noNodeAutoIncrement", action="store_true", help="If set, all nodes start at the first universe given by the universe flag.")
parser.add_argument("-p", "--protocol", choices=["Art-Net", "sACN"])
parser.add_argument("-m", "--mode", choices=["Off", "Out", "In"])
parser.add_argument("-re", "--rdmEnable", action="store_const", const=True, default=None, dest="rdmEnabled", help="Enable Art-Net RDM.")
parser.add_argument("-rd", "--rdmDisable", action="store_const", const=False, default=None, dest="rdmEnabled", help="Disable Art-Net RDM.")
parser.add_argument("--outputMode", choices=["Max", "Relaxed"])
parser.add_argument("--mergeMode", choices=["HTP", "LTP", "Off"])
parser.add_argument("--failureBehaviour", choices=["Hold", "Off"])
parser.add_argument("--autoUniLabel", choices=["Enable", "Disable"], help="Enable/Disable the Swisson Auto Universe Label")
parser.add_argument("-v", "--verbose", action="store_true", help="Enable verbose console output.")

args = parser.parse_args()

universeOffset = 0

checkAllIpsAreXnds(args.ipAddrs)

if args.print:
	for ipAddr in args.ipAddrs:
		print("-"*65 + "\n" + "-"*65)
		xndType = getXndTypeForIp(ipAddr)
		nbrOfPorts = getNumberOfPortForXndType(xndType)
		print("-"*30)
		print("{} is an {}.".format(ipAddr, xndType))
		print("-"*30)
		for i in range(0, nbrOfPorts):
			printPort(ipAddr, i + 1)
			print("-"*30)
		print("-"*65 + "\n" + "-"*65 + "\n")
else:
	# configure
	for ipAddr in args.ipAddrs:
		xndType = getXndTypeForIp(ipAddr)
		nbrOfPorts = getNumberOfPortForXndType(xndType)
		if args.verbose:
			print("-"*65 + "\n" + "-"*65)
			print("{} is an {} and has {} ports.".format(ipAddr, xndType, nbrOfPorts))
			print("Configuring {}:".format(ipAddr))
			print("-"*30)
		for i in range(0, nbrOfPorts):
			if args.protocol:
				setPortProtocol(ipAddr, i + 1, args.protocol, args.verbose)
			if args.universe is not None:
				setPortUniverse(ipAddr, i + 1, args.universe + universeOffset, args.verbose)
				if not args.noAutoIncrement:
					universeOffset += 1
			if args.mode:
				setPortMode(ipAddr, i + 1, args.mode, args.verbose)
			if args.rdmEnabled is not None:
				setPortArtNetRdmEnabled(ipAddr, i + 1, args.rdmEnabled, args.verbose)
			if args.outputMode:
				setPortOutputMode(ipAddr, i + 1, args.outputMode, args.verbose)
			if args.mergeMode:
				setPortMergeMode(ipAddr, i + 1, args.mergeMode, args.verbose)
			if args.failureBehaviour:
				setPortFailureBehaviour(ipAddr, i + 1, args.failureBehaviour, args.verbose)
			if args.autoUniLabel:
				if args.autoUniLabel == "Enable":
					setPortAutoUniLabel(ipAddr, i + 1, True, args.verbose)
				elif args.autoUniLabel == "Disable":
					setPortAutoUniLabel(ipAddr, i + 1, False, args.verbose)
		if args.verbose:
			print("-"*65 + "\n" + "-"*65 + "\n")
		if args.noNodeAutoIncrement:
			universeOffset = 0
	
