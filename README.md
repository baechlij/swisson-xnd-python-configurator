# Swisson-XND-Python-Configurator

A hacky but functional way to configure one or more Swisson XND Ethernet DMX nodes using a python commandline tool.
Note that this based on the request model used by the webinterface which is subject to change.
This can be used as a starting point to quickly reconfigure a large amount of Swisson nodes, however it is not officially supported.

Tested with XND-4 firmware version 1.04 and XND-8 firmware version 1.01.

## Requirements
* python3
* [requests](https://pypi.org/project/requests/)
* Swisson XND with webinterface enabled

## Usage
    usage: xnd-config.py [-h] [--print] [-u UNIVERSE] [--noAutoIncrement]
                         [--noNodeAutoIncrement] [-p {Art-Net,sACN}]
                         [-m {Off,Out,In}] [-re] [-rd]
                         [--outputMode {Max,Relaxed}] [--mergeMode {HTP,LTP,Off}]
                         [--failureBehaviour {Hold,Off}]
                         [--autoUniLabel {Enable,Disable}] [-v]
                         IP [IP ...]

    Remote configuration utility for Swisson XND-4 and XND-8.

    positional arguments:
      IP                    At least one IP address in the order in which they are
                            to be configured/printed.

    optional arguments:
      -h, --help            show this help message and exit
      --print               If given, the current configuration of the listed
                            nodes is printed and all other flags are ignored.
      -u UNIVERSE, --universe UNIVERSE
                            The first universe.
      --noAutoIncrement     If set, all ports are configured identical, otherwise
                            universe is incremented by 1 for each port.
      --noNodeAutoIncrement
                            If set, all nodes start at the first universe given by
                            the universe flag.
      -p {Art-Net,sACN}, --protocol {Art-Net,sACN}
      -m {Off,Out,In}, --mode {Off,Out,In}
      -re, --rdmEnable      Enable Art-Net RDM.
      -rd, --rdmDisable     Disable Art-Net RDM.
      --outputMode {Max,Relaxed}
      --mergeMode {HTP,LTP,Off}
      --failureBehaviour {Hold,Off}
      --autoUniLabel {Enable,Disable}
                            Enable/Disable the Swisson Auto Universe Label
      -v, --verbose         Enable verbose console output.
	  
	  
## Notes
* Before doing anything, the script checks that each given IP is an XND-4 or XND-8. So you don’t end up with a range of IPs that are configured and a range that isn’t.
* The “print” flag just prints the current configuration of the IPs given, so you can easily check the current configuration before doing changes or check if everything was applied as expected.
* The “-v” or “verbose” flag displays each request that is sent out for troubleshooting.
* The script might not work properly if the nodes are in Art-Net 3 mode due to the limits of the Art-Net specification.
* The options for the input mode are not available (unicast IP, etc).
* It is not possible to configure individual ports, entire nodes only. 
  
## Examples
The examples will use the following devices:

* XND-4, IP 2.0.0.101
* XND-8, IP 2.0.0.102
* XND-8, IP 2.0.0.103

### Simple start universe

    xnd-config.py -u 42 2.0.0.101 2.0.0.102 2.0.0.103

Will result in the following configuration:

**2.0.0.101 (XND-4)**
* Port 1: Universe 42
* Port 2: Universe 43
* Port 3: Universe 44
* Port 4: Universe 45

**2.0.0.102 (XND-8)**
* Port 1: Universe 46
* Port 2: Universe 47
* Port 3: Universe 48
* Port 4: Universe 49
* Port 5: Universe 50
* Port 6: Universe 51
* Port 7: Universe 52
* Port 8: Universe 53

**2.0.0.103 (XND-8)**
* Port 1: Universe 54
* Port 2: Universe 55
* Port 3: Universe 56
* Port 4: Universe 57
* Port 5: Universe 58
* Port 6: Universe 59
* Port 7: Universe 60
* Port 8: Universe 61

All other settings will be unaffected.

### Changing Protocol and RDM Enabled

    xnd-config.py -rd -p sACN 2.0.0.101 2.0.0.102 2.0.0.103

This will disable Art-Net RDM on all three nodes as well as switch the protocol to sACN (keeping the universe if possible).
All other settings will be unaffected.